/**
 *
 * \file FaceDetector.hpp
 *
 * \brief 
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include <opencv2/opencv.hpp>
#include <vector>

namespace	Bress
{
  namespace		Eyetracking
  {
    class		FaceDetector
    {

      struct	face
      {
	cv::Rect	faceROI;
	cv::Rect	eyesROI;
	cv::Rect	rightEyeCenter;
	cv::Rect	leftEyeCenter;
      };

    public:
      FaceDetector(void);
      ~FaceDetector(void);

    public:
      bool	initialize(void);
      void	detect(cv::Mat &frame, cv::Mat &left_eye, cv::Mat &right_eye);

    private:
      void	detectFaces(cv::Mat &frame);
      bool	detectEyes(cv::Mat &frame, cv::Rect &face, cv::Rect &eyesZone);
      void	detectZoneOfEyes(cv::Mat &frame, std::vector<cv::Rect> &facesROI);
      bool	checkROIValidity(const cv::Mat &frame, const cv::Rect &ROI) const;
      void	optimizedDetection(cv::Mat &frame, cv::Mat &left_eye, cv::Mat &right_eye);

    private:
      cv::CascadeClassifier	_opencvHaarFaceDetector;
      cv::CascadeClassifier	_opencvHaarEyesDetector;
      std::vector<std::pair<cv::Rect, cv::Rect> >	_faceAndEyes;
    };
  }
}
