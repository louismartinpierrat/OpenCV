/*!
 *
 * \file FaceDetector.cpp
 *
 * \brief
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include "FaceDetector.hpp"
#include "Logger.hpp"
#include "bress-eyetracking.hpp"

namespace	Bress
{
  namespace		Eyetracking
  {
    FaceDetector::FaceDetector(void)
    {
      Logger::logInfo("FaceDetector constructed.");
    }

    FaceDetector::~FaceDetector(void)
    {
      Logger::logInfo("FaceDetector destructed.");
    }

    bool
    FaceDetector::initialize(void)
    {
      Logger::logInfo("FaceDetector initialized.");
      if (this->_opencvHaarFaceDetector.load(HAAR_FACE_DETECTOR_PATH) == false) {
	Logger::logError("opencvHaarFaceDetector can NOT load : " HAAR_FACE_DETECTOR_PATH ".");
	return false;
      }
      if (this->_opencvHaarEyesDetector.load(HAAR_EYES_DETECTOR_PATH) == false) {
	Logger::logError("opencvHaarEyesDetector can NOT load : " HAAR_EYES_DETECTOR_PATH ".");
	return false;
      }
      return true;
    }

    void
    FaceDetector::detect(cv::Mat &frame, cv::Mat &left_eye, cv::Mat &right_eye)
    {
      if (this->_faceAndEyes.empty())
	this->detectFaces(frame);
      this->optimizedDetection(frame, left_eye, right_eye);
    }

    void
    FaceDetector::optimizedDetection(cv::Mat &frame, cv::Mat &left_eye, cv::Mat &right_eye)
    {
      for (std::vector<std::pair<cv::Rect, cv::Rect> >::iterator it = this->_faceAndEyes.begin() ;
	   it != this->_faceAndEyes.end() ; ++it) {
	std::vector<cv::Rect>	eyes;
	cv::Mat			frameCutted(frame, (*it).second);

	cv::rectangle(frame, (*it).first, cv::Scalar(255, 0, 0));
	cv::rectangle(frame, (*it).second, cv::Scalar(0, 255, 0));

	this->_opencvHaarEyesDetector.detectMultiScale(frameCutted, eyes);
	for (std::vector<cv::Rect>::iterator it = eyes.begin() ; it != eyes.end() ; ++it) {


	  if (it == eyes.begin())
	    left_eye = cv::Mat(frameCutted, *it);
	  else
	    right_eye = cv::Mat(frameCutted, *it);

	  cv::rectangle(frameCutted, *it, cv::Scalar(0, 0, 255));
	}
      }
    }

    bool
    FaceDetector::detectEyes(cv::Mat &frame, cv::Rect &face, cv::Rect &eyesZone)
    {
      std::vector<cv::Rect>	eyes;
      cv::Mat			frameCutted(frame, eyesZone);

      this->_opencvHaarEyesDetector.detectMultiScale(frameCutted, eyes);

      if (!eyes.empty()) {
	this->_faceAndEyes.push_back(std::pair<cv::Rect, cv::Rect>(face, eyesZone));

	for (std::vector<cv::Rect>::iterator it = eyes.begin() ; it != eyes.end() ; ++it) {
	  cv::rectangle(frameCutted, *it, cv::Scalar(0, 0, 255));
	}
	return true;
      }
      return false;
    }

    void
    FaceDetector::detectZoneOfEyes(cv::Mat &frame, std::vector<cv::Rect> &facesROI)
    {
      for (std::vector<cv::Rect>::iterator it = facesROI.begin() ;
	   it != facesROI.end() ; ) {

	if (this->checkROIValidity(frame, *it) == false) {
	  it = facesROI.erase(it);
	  continue;
	}

	cv::Rect		eyesZone = *it;

	eyesZone.y += eyesZone.height / 5;
	eyesZone.height = eyesZone.height / 3;

	cv::rectangle(frame, *it, cv::Scalar(0, 255, 0));

	if (this->detectEyes(frame, *it, eyesZone) == false)
	  it = facesROI.erase(it);
	else
	  ++it;
      }
    }

    bool
    FaceDetector::checkROIValidity(const cv::Mat &m, const cv::Rect &roi) const
    {
      if (0 <= roi.x && 0 <= roi.width && roi.x + roi.width <= m.cols &&
	  0 <= roi.y && 0 <= roi.height && roi.y + roi.height <= m.rows)
	return true;
      return false;
    }

    void
    FaceDetector::detectFaces(cv::Mat &frame)
    {
      std::vector<cv::Rect>	facesROI;

      this->_opencvHaarFaceDetector.detectMultiScale(frame, facesROI);

      for (std::vector<cv::Rect>::iterator it = facesROI.begin() ;
	   it != facesROI.end() ; ++it) {
	cv::rectangle(frame, (*it), cv::Scalar(255, 0, 0));
      }

      this->detectZoneOfEyes(frame, facesROI);

    }

  }
}
