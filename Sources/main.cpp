/*!
 *
 * \file main.cpp
 *
 * \brief Entry point of the program
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0
 *
 */

#include "Kernel.hpp"

int		main(void)
{
  Bress::Eyetracking::Kernel	app;

  return app.start();
}
