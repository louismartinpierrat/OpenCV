/*!
 *
 * \file Logger.cpp
 *
 * \brief Implementation of Logger.hpp class, used to log information into a log file.
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include "Logger.hpp"
#include <ctime>

#include <iostream>

namespace	Bress
{
  namespace		Eyetracking
  {
    std::ofstream	Logger::file("./BressEyetracking.log");


    void
    Logger::logInfo(const std::string &toLog)
    {
      time_t	rawtime = time(NULL);
      
      file << "[" << changeDateFormat(asctime(localtime(&rawtime))) << "]"
	   << "[INFO] : "
	   << toLog
	   << std::endl;
    }

    void
    Logger::log(const std::string &toLog)
    {
      time_t	rawtime = time(NULL);

      file << "[" << changeDateFormat(asctime(localtime(&rawtime))) << "] : "
	   << toLog
	   << std::endl;
    }

    void
    Logger::logError(const std::string &toLog)
    {
      time_t	rawtime = time(NULL);
      
      file << "[" << changeDateFormat(asctime(localtime(&rawtime))) << "]"
	   << "[ERROR] : "
	   << toLog
	   << std::endl;
    }

    char *
    Logger::changeDateFormat(char *date)
    {
      date[24] = 0;
      return date;
    }
  }
}
