/*!
 *
 * \file Logger.hpp
 * 
 * \brief 
 * 
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include <fstream>
#include <string>

namespace	Bress
{
  namespace	Eyetracking
  {
    class	Logger
    {
    public:
      static void		log(const std::string &toLog);
      static void		logError(const std::string &toLog);
      static void		logInfo(const std::string &toLog);

    private:
      static char		*changeDateFormat(char *date);

    private:
      static std::ofstream	file;
    };
  }
}
