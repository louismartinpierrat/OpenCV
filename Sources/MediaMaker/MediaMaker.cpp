/*!
 *
 * \file MediaMaker.cpp
 *
 * \brief This file contains the implementation of the MediaMaker class
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include "MediaMaker.hpp"
#include "Logger.hpp"
#include "bress-eyetracking.hpp"

namespace	Bress
{
  namespace		Eyetracking
  {
    MediaMaker::MediaMaker(void) :
      _camera(0),
      _rightEyeVideoWriter(RIGHT_EYE_VIDEO_FILENAME),
      _leftEyeVideoWriter(LEFT_EYE_VIDEO_FILENAME)
    {
      Logger::logInfo("MediaMaker constructed.");
    }

    MediaMaker::~MediaMaker(void)
    {
      Logger::logInfo("MediaMaker destructed.");
    }

    bool
    MediaMaker::initialize(void)
    {
      if (this->_faceDetector.initialize() == false) {
	Logger::logError("[MEDIAMAKER] FaceDetector can NOT be initialized.");
	return false;
      }
      return true;
    }

    void
    MediaMaker::launch(void)
    {
      Logger::logInfo("MediaMaker launched.");
      cv::Mat	frame, rightEye, leftEye;

      int	key;
      cv::namedWindow("Debug");

      while (this->_camera.read(frame))
	{
	  this->_faceDetector.detect(frame, rightEye, leftEye);
	  this->_rightEyeVideoWriter.write(rightEye);
	  this->_leftEyeVideoWriter.write(leftEye);

	  cv::imshow("Debug", frame);
	  key = cv::waitKey(10);
	  if (key == 1048586)
	    break;
	}
      Logger::logInfo("MediaMaker stopped.");
    }
  }
}
