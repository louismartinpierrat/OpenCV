/*!
 *
 * \file MediaMaker.hpp
 *
 * \brief This file contains the description of the MediaMaker class
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include "MP4VideoWriter.hpp"
#include "FaceDetector.hpp"

namespace	Bress
{
  namespace		Eyetracking
  {
    /*!
     *
     * \class MediaMaker
     *
     * \brief This class is used to generate the two video stream of the patient eyes.
     * 
     */
    class			MediaMaker
    {
    public:
      MediaMaker(void);
      ~MediaMaker(void);

    public:
      bool	initialize(void);
      void	launch(void);


    private:
      cv::VideoCapture	_camera;
      FaceDetector	_faceDetector;
      MP4VideoWriter	_rightEyeVideoWriter;
      MP4VideoWriter	_leftEyeVideoWriter;
    };
  }
}
