/*!
 *
 * \file Kernel.cpp
 *
 * \brief C++ implementation of the class defined in the Kernel.hpp file.
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0
 *
 */

#include "bress-eyetracking.hpp"
#include "Kernel.hpp"
#include "Logger.hpp"

namespace	Bress
{
  namespace	Eyetracking
  {
    Kernel::Kernel(void)
    {
      Logger::log("Bress-Eyetracking " BRESS_EYETRACKING_VERSION " started.");
    }

    Kernel::~Kernel(void)
    {
      Logger::log("Bress-Eyetracking closed.");
    }

    bool
    Kernel::initialize(void)
    {
      if (this->_cameraInput.open(0) == false) {
	std::cerr << "[BRESS][EYETRACKING][KERNEL] : VideoCapture can't access camera" << std::endl;
	return false;
      }
      if (this->_rightEyeDetector.load(HAAR_RIGHT_EYE_DETECTOR_PATH) == false) {
	std::cerr << "[BRESS][EYETRACKING][KERNEL] :"
	  " right eye detector can't be initialized."<< std::endl;
	return false;
      }
      if (this->_leftEyeDetector.load(HAAR_LEFT_EYE_DETECTOR_PATH) == false) {
	std::cerr << "[BRESS][EYETRACKING][KERNEL] :"
	  " left eye detector can't be initialized."<< std::endl;
	return false;
      }
      if (this->_faceDetector.load(HAAR_FACE_DETECTOR_PATH) == false) {
	std::cerr << "[BRESS][EYETRACKING][KERNEL] :"
	  " face detector can't be initialized." << std::endl;
	return false;
      }
      if (this->_eyesDetector.load(HAAR_EYES_DETECTOR_PATH) == false) {
	std::cerr << "[BRESS][EYETRACKING][KERNEL] :"
	  " face detector can't be initialized." << std::endl;
	return false;
      }
      if (this->_outputRightEye.open("./video/right_eye.avi",
				     cv::VideoWriter::fourcc('M','J','P','G'),
				     10,
				     cv::Size(this->_cameraInput.get(cv::CAP_PROP_FRAME_WIDTH),
					      this->_cameraInput.get(cv::CAP_PROP_FRAME_HEIGHT))
				     ) == false) {
	std::cout << "Can't open right_eye.mpeg for writing" << std::endl;
	return false;
      }
      cv::namedWindow("FaceRecognition", 1);
      cv::namedWindow("EyesRecognition", 1);
      return true;
    }

    bool
    Kernel::faceVerification(cv::Mat &frame, cv::Rect &eyesPosition)
    {
      std::vector<cv::Rect>	eyes;

      this->_eyesDetector.detectMultiScale(frame, eyes);
      for (std::vector<cv::Rect>::iterator it = eyes.begin() ; it != eyes.end() ; ++it) {
	if (it == eyes.begin()) {
	  eyesPosition = *it;
	} else {

	  if (eyesPosition.x > (*it).x) {
	    eyesPosition.width += eyesPosition.x - (*it).x;
	    eyesPosition.x = (*it).x;
	  }
	  if (eyesPosition.x + eyesPosition.width < (*it).x + (*it).width) {
	    eyesPosition.width = (*it).x + (*it).width - eyesPosition.x;
	  }
	  if (eyesPosition.y > (*it).y) {
	    eyesPosition.height = eyesPosition.height + eyesPosition.y - (*it).y;
	    eyesPosition.y = (*it).y;
	  }
	  if (eyesPosition.y + eyesPosition.height < (*it).y + (*it).height) {
	    eyesPosition.height = (*it).y + (*it).height - eyesPosition.y;
	  }
	}
      }
      eyesPosition.x -= MARGING;
      eyesPosition.y -= MARGING;
      eyesPosition.width += MARGING * 2;
      eyesPosition.height += MARGING * 2;
      return !eyes.empty();
    }

    void
    Kernel::eyesDetection(cv::Mat &frame, std::vector<cv::Rect> &regionsOfInterests)
    {
      std::vector<cv::Rect>     eyes;
      int			consecutiv = 0;
      if (regionsOfInterests.empty())
	return;
      do {
	for (std::vector<cv::Rect>::const_iterator it = regionsOfInterests.begin() ; it != regionsOfInterests.end() ; ++it) {
	  cv::Mat	frameOfROI(frame, (*it));

	  this->_eyesDetector.detectMultiScale(frameOfROI, eyes);

	  if (eyes.size() < 2 && ++consecutiv >= 5) {
	    std::cout << "je quitte la fonction de detection des yeux" << std::endl;
	    return;
	  }
	  else if (eyes.size() >= 2) {
	    std::cout << "consecutiv :" << consecutiv << std::endl;
	    consecutiv = 0;
	  }

	  for (std::vector<cv::Rect>::const_iterator it = eyes.begin() ; it != eyes.end() ; ++it) {
	    cv::rectangle(frameOfROI, (*it), cv::Scalar(255, 0, 0));
	  }
	  this->_outputRightEye.write(frame);
	  cv::waitKey(10);
	  cv::imshow("FaceRecognition", frame);
	}
      } while (this->_cameraInput.read(frame));
    }

    void
    Kernel::optimizeRegionsOfInterests(cv::Mat &frame, std::vector<cv::Rect> &regionsOfInterests)
    {
      for (std::vector<cv::Rect>::iterator it = regionsOfInterests.begin() ;
	   it != regionsOfInterests.end() ;
	   ++it) {
	cv::Mat       frameOfROI(frame, (*it));
	cv::Rect	newROI;

	std::cout << "FACE FOUND" << std::endl;
	cv::rectangle(frame, (*it), cv::Scalar(255, 0, 0));

	if (faceVerification(frameOfROI, newROI) == false)
	  it = regionsOfInterests.erase(it);
	else {

	  cv::rectangle(frameOfROI, newROI, cv::Scalar(0, 255, 0));
	  
	  (*it).width = newROI.width;
	  (*it).height = newROI.height;
	  (*it).x += newROI.x;
	  (*it).y += newROI.y;

	  cv::rectangle(frame, (*it), cv::Scalar(0, 0, 255));
	}
	cv::imshow("Debug", frame);
      }
    }

    void
    Kernel::faceDetection(void)
    {
      std::vector<cv::Rect>     regionsOfInterests;
      cv::Mat			frame;

      while (this->_cameraInput.read(frame)) {
	this->_faceDetector.detectMultiScale(frame, regionsOfInterests);
	this->optimizeRegionsOfInterests(frame, regionsOfInterests);
	this->eyesDetection(frame, regionsOfInterests);
      }
    }

    int
    Kernel::start(void)
    {
      // if (this->initialize() == false)
      // 	return EXIT_FAILURE;

      this->_mediaMaker.initialize();
      this->_mediaMaker.launch();
      /* 
	 this->_httpServer.launch();
       */
      // this->faceDetection();
      return EXIT_SUCCESS;
    }
  }
}
