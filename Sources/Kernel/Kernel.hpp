/*!
 *
 * \file Kernel.hpp
 *
 * \brief This class is the main part of the projec.t
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0
 *
 */

#ifndef		__KERNEL_HPP__
# define	__KERNEL_HPP__

#include "MediaMaker.hpp"
#include <opencv2/opencv.hpp>
#include <vector>

namespace	Bress
{
  namespace	Eyetracking
  {
    /*!
     *
     * \class Kernel
     *
     * \brief This class is used to load component require by the program and launch them.
     *
     * This class will launch HTTPServer Module and MediaMaker Module.
     * It will also initialize Logger
     *
     */
    class		Kernel
    {
    public:
      /*
       *
       * \brief Constructor
       *
       * ...
       *
       */
      Kernel(void);

      /*
       *
       * \brief Destructor
       *
       */
      ~Kernel(void);

    public:
      /*
       *
       * \brief start method
       *
       */
      int		start(void);

    private:
      /*
       *
       * \brief This function is used to initialize kernel class, automaticaly called by start method.
       *
       */
      bool		initialize(void);

      /*
       *
       * \brief ...
       *
       */
      bool		faceVerification(cv::Mat &frame, cv::Rect &eyesPosition);

      void		faceDetection(void);

      void		optimizeRegionsOfInterests(cv::Mat &, std::vector<cv::Rect> &);

      void		eyesDetection(cv::Mat &, std::vector<cv::Rect> &);
      
    private:

      MediaMaker	_mediaMaker;

      cv::VideoWriter	_outputRightEye;

      /*
       *
       * \brief ...
       *
       */
      cv::VideoCapture	_cameraInput;

      /*
       *
       * \brief ...
       *
       */
      cv::CascadeClassifier	_faceDetector;

      /*
       *
       * \brief ...
       *
       */
      cv::CascadeClassifier	_eyesDetector;

      /*
       *
       * \brief ...
       *
       */
      cv::CascadeClassifier	_rightEyeDetector;

      /*
       *
       * \brief ...
       *
       */
      cv::CascadeClassifier	_leftEyeDetector;
    };
  }
}

#endif /* !__KERNEL_HPP__ */

