/*!
 *
 * \file MP4VideoWriter.cpp
 *
 * \brief 
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include	"MP4VideoWriter.hpp"
#include	"Logger.hpp"

namespace	Bress
{
  namespace		Eyetracking
  {
    MP4VideoWriter::MP4VideoWriter(const std::string &filename) :
      _filename(filename)
    {
      Logger::logInfo("MP4VideoWriter constructed with : " + filename + " as filename.");
      // if (this->_tmp.open(this->_filename,
      // 			  cv::VideoWriter::fourcc('M','J','P','G'),
      // 			  10,
      // 			  cv::Size(500, 300)) == false) {
	// Logger::logError("CANNOT OPEN MJPG fourcc file.");
      // }
      std::cout << "Open : " << filename << std::endl;
      this->_tmp.open("./video/output_" + filename + ".fifo");
      if (this->_tmp.is_open() == false) {
	std::cerr << "Can't open fifo" << std::endl;
      }
    }

    MP4VideoWriter::~MP4VideoWriter(void)
    {

      Logger::logInfo("MP4VideoWriter using " + this->_filename + " destructed.");
    }

    void
    MP4VideoWriter::write(cv::Mat &frame)
    {
      cv::Mat	newFrame(cv::Size(500, 300), frame.type());

      cv::resize(frame, newFrame, newFrame.size(), 0, 0, cv::INTER_CUBIC);
      this->_tmp << newFrame;
      cv::imshow(this->_filename, newFrame);
    }

  }
}
