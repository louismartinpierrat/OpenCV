/*!
 *
 * \file MP4VideoWriter.hpp
 *
 * \brief
 *
 * \author Louis Martin-Pierrat
 *
 * \version 1.0.0
 * 
 */

#include <string>
#include <fstream>
#include <opencv2/opencv.hpp>

namespace	Bress
{
  namespace		Eyetracking
  {
    class		MP4VideoWriter
    {
    public:
      MP4VideoWriter(const std::string &filename);
      ~MP4VideoWriter(void);

    public:
      void	write(cv::Mat &frame);

    private:
      std::string	_filename;
      // cv::VideoWriter	_tmp;
      std::ofstream	_tmp;
    };
  }
}
